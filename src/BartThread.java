import java.util.*;
import java.io.*;
import java.net.*;

public class BartThread implements Runnable
{
    private Socket s;
    private BartQuote bart;

    public BartThread(){}

    public BartThread(Socket s, BartQuote bart)
    {
        this.bart = bart;
        this.s = s;
    }

    public void run()
    {
        try
        {
            String host = s.getInetAddress().toString();
            System.out.println("Connected to  : " + host);

            Scanner in = new Scanner(s.getInputStream());
            PrintWriter pw = new PrintWriter(s.getOutputStream(), true);

            String command = "";

            pw.println("Welcome to BartServer 1.0");
            pw.println("Enter GET to get a quote " + "or BYE to exit.");

            while(true)
            {
                //pw.println("Enter the command : ");
                command = in.nextLine();

                if(command.equals("get"))
                {
                    pw.println(bart.getQuote());
                    System.out.println("Serving " + host);
                }
                else if(command.equals("bye"))
                {
                    break;
                }
                else
                {
                    pw.println("Command Invalid");
                }
            }

            pw.println("Good Bye!");

            s.close();

            System.out.println("Closed connection to " + host);

        }
        catch(Exception e){e.printStackTrace();}
    }
}