import java.net.*;
import java.io.*;

public class Program
{
    public Program()
    {
        int port = 5050;

        try
        {
            BartQuote bart;
            ServerSocket ss = new ServerSocket(port);
            Socket s = null;

            System.out.println("BartServer 1.0");
            System.out.println("Listening on port " + port);

            while (true)
            {
                s = ss.accept();
                bart = new BartQuote();

                Thread t = new Thread(new BartThread(s, bart));
                t.start();
            }
        }
        catch(Exception e){e.printStackTrace();}
    }

    public static void main(String[] args)
    {
        new Program();
    }
}
